# Ansible Playbook for Django using CentOS 7, nginx, and PostgreSQL

## TODO

Add Fail2Ban

## Prerequisites

Create and inventory.ini file

```
[<project_name>]
<project_url>

[<project_name>:vars]
remote_user=<remote user>
project_name=<project name>
email_address=<email address>
full_name=<Full Name>
django_email=<django email address>
django_email_password=<django email password>
www=False # True if url can start with www
```

Add ssh key to all remote servers

```
# ssh-copy-id root@<project_url>
```

## How to run

```
# ansible-playbook all.py
```


## Extra

Add password for Django superuser using expect:

https://gist.github.com/elleryq/9c70e08b1b2cecc636d6
